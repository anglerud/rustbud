rustbud, an alternative Rust toolchain manager
==============================================

rustbud is a tool to download and set up a Rust build environment
(including `rustc`, `cargo`, documentation, and various other tools).
It's designed to follow Unix tool conventions, so it can easily integrate
with larger systems. It also follows the "shared XOR mutable" philosophy,
so you can as many different environments as you like and they'll share
what they can, but changes made to one environment will be isolated from
the others, even if things from different environments are running
simultaneously.

rustbud uses the official binaries hosted by the Rust project.

Reasons not to use rustbud
--------------------------

You might want to avoid rustbud if you want a toolchain manager that:

  - is mature and trusted
      - rustbud barely works yet
  - matches what everybody else uses
      - [rustup][rustup] is the Official Toolchain Manager, and probably
        always will be
  - works the same way on all platforms
      - rustbud is designed to be Unixy, and will probably be
        clunky-at-best on non-Unix platforms if it works at all.
  - validate downloads against their GPG signatures
      - Talking to GPG is Hard
  - works with multiple build environments from within a single tool,
    like an IDE.
      - rustbud works by setting environment variables, and an environment
        variable can only be set to a single value.
  - installs the Rust toolchain on `$PATH`, so you can get started right away
      - rustbud requires you to "activate" an environment before using it,
        there's no "default environment".
  - has minimal dependencies, so you can set it up quickly on a new machine.
      - The current version of rustbud is written in Python, which may
        require its own toolchain manager to set up.

[rustup]: https://www.rustup.rs/

Installation
============

Right now, the current experimental version of rustbud is written in Python.
Hopefully that will change soon, but in the mean time...

Requirements
------------

 - Python 3.5 or above
 - `~/.local/bin` should be on your `$PATH`

Installing with Pip
-------------------

All modern Python installs should come with [Pip][pip], the Python
packaging tool. The easiest way to get rustbud installed is:

    pip3 install --user https://gitlab.com/Screwtapello/rustbud/repository/master/archive.zip

[pip]: https://pip.pypa.io/en/stable/

To uninstall rustbud with Pip:

    pip3 uninstall rustbud

Note that this won't uninstall any of rustbud's dependencies, and requires
that you use the same version of Python that you installed with.

Installing with Pipsi
---------------------

[Pipsi][pipsi] is a wrapper around Pip specifically for installing
command-line tools like rustbud, which keeps them and their dependencies
isolated from each other and from any other Python libraries or tools
you might have installed. I recommend installing rustbud with Pipsi.

    # gotta bootstrap somehow :/
    curl https://raw.githubusercontent.com/mitsuhiko/pipsi/master/get-pipsi.py | python3

    pipsi install https://gitlab.com/Screwtapello/rustbud/repository/master/archive.zip#egg=rustbud

To uninstall rustbud with Pipsi:

    pipsi uninstall rustbud

[pipsi]: https://github.com/mitsuhiko/pipsi

Using rustbud
=============

To try out rustbud for the first time, make a temporary directory and run
`rustbud-activate`:

    $ mkdir ~/rustbud-demo
    $ cd ~/rustbud-demo
    $ rustbud-activate

You'll see rustbud print a lot of messages about downloading and
installing things, and then drop you back to the shell prompt. However,
this is not the original shell you started from, this is a subshell
configured to use the Rust toolchain you just downloaded:

    $ which rustc
    ~/rustbud-demo/rustbud-env/bin/rustc

(it's probably going to print the actual path to your home directory,
rather than abbreviating it to `~`)

From this shell, you can use `rustc` and `cargo` as normal. If you run
`cargo install`, the program will be installed into this custom
environment, isolated from other environments and your normal system
configuration.

    $ cargo install ripgrep
    $ which rg
    ~/rustbud-demo/rustbud-env/bin/rg

Once you're done using this environment, you can go back to your original
shell by exiting the subshell:

    $ exit

If you later need to activate this environment again, just run
`rustbud-activate` in the same directory. Since the environment was
already set-up, it doesn't need to download or install things again.

If you want to run a particular command inside the environment,
rather than an interactive shell, you can put it at the end of the
`rustbud-activate` command line:

    $ rustbud-activate cargo --version
    cargo 0.21.0 (5b4b8b2ae 2017-08-12)

rustbud activates the environment for the duration of the command. The
command's exit-code is passed through unmodified, so you can use the
`rustbud-activate`-wrapped command pretty much anywhere you'd have used
the original command.

Choosing a host triple
----------------------

When rustbud chooses a toolchain to download, it tries to pick one that
will run on your particular computer and operating system. Rust uses a
special name called (for obscure reasons) a "triple" to distinguish
the available variants. rustbud has some very limited logic for guessing
which triple to use, but if it guesses incorrectly (or you deliberately
want a different version for some reason) you can use the `--host-triple`
option when you first create the environment:

    rustbud-activate --host-triple i686-unknown-linux-gnu

Some common triples are:

  - **x86_64-unknown-linux-gnu** for most Linux environments
  - **i686-unknown-linux-gnu** for old, 32-bit Linux environments
  - **x86_64-apple-darwin** for most macOS environments
  - **i686-apple-darwin** for old, 32-bit macOS environments

Note that "host triple" describes the environment where the compiler
runs. "target triple" describes the environment where the *output*
of the compiler runs.

Specs, Locks and Envs
---------------------

Conceptually, rustbud works like this:

  - rustbud reads the `rustbud-spec.toml` file in the current directory,
    which specifies the Rust release channel you want to use (stable,
    nightly, etc.) and what components from that channel to install
    (rustc, cargo, docs, etc.).
  - rustbud downloads the manifest for the requested channel and figures out
    which downloads will provide the requested components.
  - rustbud writes the chosen list of downloads to `rustbud-lock.json`
    in the same directory as `rustbud-spec.toml`.
  - rustbud downloads all the files listed in `rustbud-lock.json` and
    installs them into a new directory named `rustbud-env`.

`rustbud-activate` will create `rustbud-env` if it doesn't already exist,
based on `rustbud-lock.json`. If that doesn't exist either, it will
be created based on `rustbud-spec.toml`. If that also doesn't exist,
it will use sensible defaults (stable channel, and all the components
that the stable channel marks "required").

Therefore, if you have a project with no special requirements, you can
use rustbud with no extra ceremony. If you have a project that requires
Rust nightlies, you can create a `rustbud-spec.toml` that specifies the
Nightly channel. Much like `Cargo.lock`, you may choose to commit your
`rustbud-lock.json` to source-control, or ignore it, depending on how
much you care about reproducing the exact build environment for historical
versions of your software.

All these names can be overridden at activation-time, but the default
names for the lock file and env directory are derived from the spec
name so that you can have multiple specs in the same directory without
their locks or envs conflicting. For example, if you want to have a
separate build-environment for development and for deployment, you might
create `devel-spec.toml` and `deploy-spec.toml`. Activating
`devel-spec.toml` will create `devel-lock.json` and `devel-env`,
while activating `deploy-spec.toml` will create `deploy-lock.json` and
`deploy-env`.

Reference
=========

Spec files
----------

A rustbud spec file uses [TOML][toml] syntax, and supports the following
configuration options:

  - `toolchain.channel` (string) specifies which Rust release channel to
    use. Default is `stable`. It may be:
      - one of the strings `stable`, `beta` or `nightly`, meaning the
        official Rust release channels.
      - An absolute or relative filesystem path to a channel manifest file.
        Relative paths are interpreted relative to the path of the spec file.
      - A URL with the `http` or `https` schemes.
  - `toolchain.formats` (list of strings) is an ordered list of archive
    formats. When a component is available in multiple formats, rustbud will
    choose the one whose format appears earliest in this list. Default:
    `["xz", "gz"]`.
  - `toolchain.required.NAME` (list of strings) tells rustbud to also
    install the component `NAME` for all the listed triples. For some
    components (like "rls") the relevant triple depends on the host where
    you're running rustbud, and for some components (like "rust-src")
    triples aren't relevant at all, so you can use shell glob patterns
    like "*" to install all variants of a component whose triples match
    the pattern. If the component `NAME` is not listed in the channel
    manifest, or the listed targets are not available for it, rustbud
    will fail to build the lock file.
  - `toolchain.optional.NAME` (list of strings) is like
    `toolchain.required.NAME` except that unknown or unavailable components
    are ignored.

Example:

```toml
[toolchain]
channel = "nightly"

[toolchain.required]
# We want to be able to build static Linux executables
rust-std=["x86_64-unknown-linux-musl"]

[toolchain.optional]
# We want to use the Rust Language Server if it's available.
rls=["*"]
```

[toml]: https://github.com/toml-lang/toml

TODO
====

Some of the things I want to add to rustbud in the future:

  - Rewrite It In Rust™
  - lock files should record the format of each artifact (gz/xz) as declared
    in the channel file, so rustbud doesn't have to guess.
  - lock files should not assume a 1:1 correspondance of artifacts
    and components.
      - maybe even decide whether to download the omnibus archive or
        individual components, based on whichever is smaller.
  - support a list of things to "cargo install" as well as the toolchain
      - this will require implementing cargo's version comparison syntax,
        and applying it to the list in
        `https://crates.io/api/v1/crates/$PKG/versions` so we can record
        a specific version in our lock file.
  - locally cache channel manifests, not just downloaded archives
  - support ignoring normally-required components in case you really don't
    need them for whatever reason (perhaps you don't need to download
    the documentation on a build-farm).
  - Mention where we're caching downloaded components on stderr
  - a standard config file to define the URLs used for the "stable",
    "beta" and "nightly" keywords
  - config options to specify how the lock-file and env-dir names are
    built from the spec-file name.
      - this will likely require some kind of runtime templating system,
        which is potentially a lot of complexity for a small feature.
  - I think RLS needs an environment variable telling it where to find
    the Rust source code; we should set that by default, whatever it is.
  - support custom envvars in `rustbud-spec.toml`.
    These will need to be saved to a file in the env, copied to the lock
    and probably to a file in the env, too.
  - Automatically rebuild the env if it's older than the lock file
  - Automatically rebuild the lock file if it's older than the spec file.
  - when creating the env, as the last step create some flag file inside
    it, which we can use to detect partially-built envs and also we can
    use its mtime for staleness-checking.
  - An option to error-out if `rustbud-activate` finds `$RUSTBUD_ROOT`
    in the environment (i.e. activating one env inside another)
  - Document how to display the currently-active environment in your
    bash prompt. Also, load the bash completion config that gets dumped
    into the `etc/` directory in the environment.
  - GPG-validate downloaded artifacts, don't just rely on validating
    their unsigned hashes.
  - Add more uname-to-triple smarts, at least for 64-bit and 32-bit
    macOS and Linux.
  - Investigate the viability of a Windows port. We can't use hardlinks,
    so it'll be slower, and "set up environment variables" might not even
    be useful under Windows, but we should at least have a go.
  - Should environments be created in `~/.cache/` rather than beside
    the spec and lock? That would make it more likely that we could
    use hard-links, and keep some cruft out of working directories, but
    generating a unique, meaningful name for every possible environment
    is Difficult.
      - at least with the current scheme, it's obvious where the
        environment lives, and `git clean -dxf` will clean it up along
        with everything else.
  - Export an environment variable `$RUSTBUD_SPEC` that points to the
    spec file whose contents specified the current environment (even if
    the actual file doesn't exist). Since you can have multiple spec files
    in the same directory, `$RUSTBUD_ROOT` is not enough.
