from setuptools import setup

setup(
    name="rustbud",
    version="0.0.1",
    packages=["rustbud"],
    install_requires=[
        "attrs>=17.2.0",    # CalVer
        "boltons>=17.1.0",  # CalVer
        "pyxdg~=0.25",
        "requests~=2.14",
        "toml~=0.9.2",
        "tqdm~=4.14",
    ],
    entry_points={
        "console_scripts": [
            "rustbud-list-artifact = rustbud.tools:list_artifact",
            "rustbud-list-channel = rustbud.tools:list_channel",
            "rustbud-activate = rustbud.tools:activate",
        ],
    },
)
