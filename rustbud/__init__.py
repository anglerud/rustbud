import errno
import json
import logging
import os
import pathlib
import shutil
import sys

import requests
import toml
import tqdm
from xdg import BaseDirectory

import rustbud.cache
import rustbud.channel
import rustbud.spec
import rustbud.lock

BASE_URL = "https://static.rust-lang.org/dist/"

CHANNEL_METADATA_URLS = {
    "stable": BASE_URL + "channel-rust-stable.toml",
    "beta": BASE_URL + "channel-rust-beta.toml",
    "nightly": BASE_URL + "channel-rust-nightly.toml",
}


LOGGER = logging.getLogger(__name__)


class Environment:
    """
    A real, actual Rust environment.

    Where most of the other classes in this codebase are mathematically-pure
    manipulations of abstract state, this deals with actual files and
    directories on disk.
    """

    def __init__(self, host_triple, spec_path, lock_file_path, env_path):
        self.host_triple = host_triple
        self.spec_path = spec_path
        self.lock_file_path = lock_file_path
        self.env_path = env_path

    def _read_maybe_missing(self, path):
        try:
            return path.read_text()

        except FileNotFoundError:
            LOGGER.warn("Path %r not found, pretending it's empty", path)

        return ""

    def _read_channel(self, maybe_url, relative_to):
        # Expand our channel keywords.
        maybe_url = CHANNEL_METADATA_URLS.get(maybe_url, maybe_url)

        # Let's check for a local file first, because that's easiest.
        try:
            return relative_to.joinpath(maybe_url).read_text()
        except IOError:
            LOGGER.info("Channel %r is not a file path, trying as a URL", maybe_url)
            pass

        # Maybe it really is a URL.
        response = requests.get(maybe_url)
        response.raise_for_status()
        return response.text

    def lock(self):
        """
        Lock this environment, picking specific artifacts to match the spec.
        """
        # Load the env spec, if it exists.
        spec_data = self._read_maybe_missing(self.spec_path)
        spec = rustbud.spec.Spec.from_file_data(
            toml.loads(spec_data)
        )

        # Figure out which channel we're supposed to analyze.
        channel_data = self._read_channel(
            spec.channel_url,
            self.spec_path.parent,
        )
        channel = rustbud.channel.Channel.from_manifest(
            toml.loads(channel_data)
        )

        # Lock the requested artifacts from the given channel.
        lock = rustbud.lock.Lock.from_spec(
            spec,
            channel,
            self.host_triple,
        )

        with self.lock_file_path.open("w") as handle:
            json.dump(lock.to_data(), handle)


    def _download_and_validate_artifact(self, cache, artifact_ref):
        base_args = {
            "unit": "B",
            "unit_scale": True,
            "bar_format": "{desc:>13s}: {bar} {n_fmt:>5s}/{total_fmt:<5s}",
        }

        # First, try verifying the previous download.
        with tqdm.tqdm(desc="Verifying", **base_args) as pb:
            try:
                for (delta, total) in cache.validate(artifact_ref):
                    pb.total = total
                    pb.update(delta)

                # Validation succeeded, we're done here.
                return

            except FileNotFoundError:
                # Not previously downloaded, or the previous download
                # was corrupt. Let's replace the Validating progress bar
                # with a Downloading one.
                pb.leave = False

        # Validation failed, let's try downloading.
        with tqdm.tqdm(desc="Downloading", **base_args) as pb:
            for (delta, total) in cache.store(artifact_ref):
                pb.total = total
                pb.update(delta)


    def _download_and_validate_artifacts(self, cache, lock):
        LOGGER.info("Checking downloaded artifacts...")
        for artifact_ref in sorted(lock):
            print("Artifact {!r}".format(artifact_ref.url), file=sys.stderr)
            self._download_and_validate_artifact(cache, artifact_ref)

    def _install_files(self, pairs):
        try_hard_links = True

        for src, dst in pairs:
            dst.parent.mkdir(parents=True, exist_ok=True)

            # We can't just symlink, because rustc reads through
            # symlinks when looking for the standard library.
            if try_hard_links:
                try:
                    os.link(str(src), str(dst))

                except OSError as e:
                    if e.errno != errno.EXDEV:
                        raise
                    LOGGER.warn(
                        "Can't install artifacts with hard links, "
                        "falling back to copying."
                    )
                    try_hard_links = False

            if not try_hard_links:
                shutil.copy(str(src), str(dst))

    def _install_artifacts(self, cache, lock):
        pbar_args = {
            "unit": "",
            "unit_scale": True,
            "bar_format": "{desc:>13s}: {bar} {n_fmt:>5s}/{total_fmt:<5s}",
        }

        try_hard_links = True

        for artifact_ref in sorted(lock):
            archive = cache.get(artifact_ref)
            for component in sorted(archive):
                print("Component:", component, file=sys.stderr)
                file_set = archive[component]
                install_pairs = [
                    (
                        file_set.base_path / each, # source
                        self.env_path / each, # destination
                    )
                    for each in sorted(file_set.file_paths)
                ]

                self._install_files(
                    tqdm.tqdm(install_pairs, desc="Installing", **pbar_args)
                )

    def build(self):
        """
        Build this environment, from the packages in the lock file.

        Packages will be downloaded as necessary.
        """
        if not self.lock_file_path.exists():
            LOGGER.warn(
                "Lock file %r does not exist, creating from env spec...",
                self.lock_file_path,
            )
            self.lock()

        with self.lock_file_path.open() as handle:
            lock = rustbud.lock.Lock.from_data(json.load(handle))

        cache = rustbud.cache.Cache(
            pathlib.Path(BaseDirectory.save_cache_path("rustbud", "downloads"))
        )

        self._download_and_validate_artifacts(cache, lock)

        self._install_artifacts(cache, lock)

        # Cargo caches the crates.io registry in a directory inside
        # $CARGO_HOME. Let's make every environment share a single copy
        # of the registry cache so they'll keep each other up-to-date.
        reg_path = pathlib.Path(
            BaseDirectory.save_cache_path("rustbud", "registry")
        )
        reg_path.mkdir(parents=True, exist_ok=True)
        (self.env_path / "registry").symlink_to(reg_path)

    def activate(self, command):
        """
        Executes the given command inside this environment.

        The environment will be created if necessary.

        The command is passed to ``os.execvp``, so this function does
        not return (unless an error occurred).
        """
        if not self.env_path.exists():
            LOGGER.warn(
                "Environment path %r does not exist, creating from lock...",
                self.env_path,
            )
            self.build()

        # Set the environment variables for the new env.
        os.environ["CARGO_HOME"] = str(self.env_path)
        os.environ["RUSTBUD_ROOT"] = str(self.env_path)
        os.environ["PATH"] = (
            str(self.env_path / "bin") +
            os.pathsep +
            os.environ["PATH"]
        )

        # Launch the command in the environment
        LOGGER.info(
            "Environment %r launching command %r",
            self.env_path,
            command,
        )
        os.execvp(command[0], command)
