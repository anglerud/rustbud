import os
import pathlib
import unittest

import rustbud.tools


class TestGuessHostTriple(unittest.TestCase):

    def build_fake_uname(self, kernel, arch):
        return os.uname_result(
            [
                kernel,
                "mycoolnode",
                "1.2.3",
                "why is the version in the release field?",
                arch,
            ]
        )

    def test_linux_x86(self):
        self.assertEqual(
            rustbud.tools.guess_host_triple(
                self.build_fake_uname("Linux", "x86_64"),
            ),
            "x86_64-unknown-linux-gnu",
        )

    def test_unknown(self):
        self.assertRaises(
            ValueError,
            rustbud.tools.guess_host_triple,
            self.build_fake_uname("My Fake OS", "6502"),
        )


class TestGuessArtifactNames(unittest.TestCase):

    def test_default(self):
        spec_path = pathlib.Path("/path/to/rustbud-spec.toml")
        self.assertEqual(
            rustbud.tools.guess_artifact_names(spec_path),
            (
                pathlib.Path("/path/to/rustbud-lock.json"),
                pathlib.Path("/path/to/rustbud-env"),
            ),
        )

    def test_custom_name(self):
        spec_path = pathlib.Path("/path/to/nightly-spec.toml")
        self.assertEqual(
            rustbud.tools.guess_artifact_names(spec_path),
            (
                pathlib.Path("/path/to/nightly-lock.json"),
                pathlib.Path("/path/to/nightly-env"),
            ),
        )

    def test_no_suffix(self):
        spec_path = pathlib.Path("/path/to/rustbudfile")
        self.assertEqual(
            rustbud.tools.guess_artifact_names(spec_path),
            (
                pathlib.Path("/path/to/rustbudfile-lock.json"),
                pathlib.Path("/path/to/rustbudfile-env"),
            ),
        )

    def test_wrong_suffix(self):
        spec_path = pathlib.Path("/path/to/rustbud-foo.bar")
        self.assertEqual(
            rustbud.tools.guess_artifact_names(spec_path),
            (
                pathlib.Path("/path/to/rustbud-foo.bar-lock.json"),
                pathlib.Path("/path/to/rustbud-foo.bar-env"),
            ),
        )

    def test_mixed_case_suffix(self):
        spec_path = pathlib.Path("/path/to/WINDOWS-SPEC.TOML")
        self.assertEqual(
            rustbud.tools.guess_artifact_names(spec_path),
            (
                pathlib.Path("/path/to/WINDOWS-lock.json"),
                pathlib.Path("/path/to/WINDOWS-env"),
            ),
        )
