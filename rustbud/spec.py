import fnmatch

import attr


@attr.s(frozen=True)
class Pattern:

    package = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
    )
    target = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
    )

    def matches(self, ref):
        return (
            ref.package == self.package
            and fnmatch.fnmatch(ref.target, self.target)
        )


def _unpack_patterns(patterns):
    for package, targets in patterns.items():
        for target in targets:
            yield Pattern(package, target)


@attr.s(frozen=True)
class Spec:

    channel_url = attr.ib(
        validator=attr.validators.instance_of(str),
        cmp=True,
        default="stable",
    )
    formats = attr.ib(
        validator=attr.validators.instance_of(tuple),
        cmp=True,
        default=("xz", "gz"),
    )
    required_components = attr.ib(
        validator=attr.validators.instance_of(frozenset),
        cmp=True,
        default=frozenset(),
    )
    optional_components = attr.ib(
        validator=attr.validators.instance_of(frozenset),
        cmp=True,
        default=frozenset(),
    )

    @classmethod
    def from_file_data(cls, manifest):
        toolchain = manifest.get("toolchain", {})

        return cls(
            channel_url=toolchain.get("channel", cls.channel_url.default),
            formats=tuple(toolchain.get("formats", cls.formats.default)),
            required_components=frozenset(_unpack_patterns(
                toolchain.get("required", {}),
            )),
            optional_components=frozenset(_unpack_patterns(
                toolchain.get("optional", {}),
            )),
        )
