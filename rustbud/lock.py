import json
import logging

import attr

import rustbud.channel


LOGGER = logging.getLogger(__name__)


class Lock(frozenset):

    @classmethod
    def from_data(cls, data):
        return cls(
            rustbud.channel.ArtifactRef.from_data(each)
            for each in data.get("toolchain")
        )

    @classmethod
    def from_spec(cls, spec, channel, host_triple):
        rust_ref = rustbud.channel.ComponentRef("rust", host_triple)
        rust = rust_ref.dereference(channel)

        artifacts = set()

        # Start with all the refs the manifest says we need.
        for ref in rust.required_components:
            comp = ref.dereference(channel)
            try:
                artifacts.add(comp.choose_artifact(spec.formats))

            except LookupError:
                raise ValueError("Package {!r} target {!r} "
                    "is not available in any acceptable format {!r}"
                    .format(ref.package, ref.target, spec.formats)
                )

        # Add all the artifacts spec requires
        for pattern in spec.required_components:
            for ref in rust.all_components:
                if pattern.matches(ref):
                    comp = ref.dereference(channel)
                    try:
                        artifacts.add(comp.choose_artifact(spec.formats))

                    except LookupError:
                        raise ValueError(
                            "Package {!r} target {!r} is not available in any "
                                "acceptable format {!r}".format(
                                    ref.package,
                                    ref.target,
                                    spec.formats,
                                ),
                        )

                    break

            else:
                raise ValueError("Package {!r} has no available targets matching {!r}"
                    .format(pattern.package, pattern.target)
                )

        # Add all the artifacts spec requests
        for pattern in spec.optional_components:
            for ref in rust.all_components:
                if pattern.matches(ref):
                    comp = ref.dereference(channel)
                    try:
                        artifacts.add(comp.choose_artifact(spec.formats))

                    except LookupError:
                        continue

                    break

            else:
                LOGGER.info(
                    "Package %r has no suitable targets matching %r",
                    pattern.package,
                    pattern.target,
                )

        return cls(artifacts)

    def to_data(self):
        return {
            "toolchain": [
                each.to_data()
                for each in sorted(self)
            ],
        }
